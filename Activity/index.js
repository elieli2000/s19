let Username;
let Password;
let Role;

function login() {
  Username = prompt("Username");
  Password = prompt("Password");
  Role = prompt("Role");




  if (!Username || !Password || !Role) {
    alert("All inputs should not be empty!");
  } else {
    switch (Role) {
      case "admin":
        alert("Welcome back to the class portal, " + Role + "!");
        break;
      case "teacher":
        alert("Thank you for logging in, " + Role + "!");
        break;

      case "student":
        alert("Welcome to the class portal, " + Role + "!");
        break;

      default:
        alert("Role out of range.");
        break;
    }
  }
}

login();


function getAverage(...numbers){
   
    let sumOfNumbers = 0;

    for(const number of numbers){
        sumOfNumbers += number;    
    }

     let average = Math.round(sumOfNumbers / numbers.length);
    console.log(average);

    if(average <= 74){
        console.log("Hello, student, your average is " +  average + ". The letter equivalent is F");
    }else if(average >= 75 && average <= 79){
        console.log("Hello, student, your average is " +  average + ". The letter equivalent is D");

    }else if(average >= 80 && average <= 84){
        console.log("Hello, student, your average is " +  average + ". The letter equivalent is C");

    }else if(average >= 85 && average <= 89){
        console.log("Hello, student, your average is " +  average + ". The letter equivalent is B");

    }else if(average >= 90 && average <= 95){
        console.log("Hello, student, your average is " +  average + ". The letter equivalent is A");

    }else {
        console.log("Hello, student, your average is " +  average + ". The letter equivalent is A+");
    }
}

getAverage(96,90,100,100);
